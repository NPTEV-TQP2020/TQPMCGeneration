#!/usr/bin/python
#
# returns the width for top mass passed as 1st script argument
# 
# run like:
# python get_twidth.py 170.0
# in order to get the top mass width for m_t=170.0 GeV
# 
# parameters used for width evaluation
# MC production common parameters (PDG2010)
# https://twiki.cern.ch/twiki/bin/view/AtlasProtected/McProductionCommonParameters
#
# references:
# http://cdsweb.cern.ch/record/1186207/files/ATL-COM-PHYS-2009-334.pdf
# [1] W. Bernreuther, Top quark physics at the LHC. J. Phys., G35:083001, 2008 [arXiv:0805.1333],
# [2] A. Czarnecki and K. Melnikov, Nucl. Phys. B 544 (1999) 520 [arXiv:hep-ph/9806244],
# [3] K. G. Chetyrkin, R. Harlander, T. Seidensticker and M. Steinhauser, Phys. Rev. D60 (1999) 114015 [arXiv:hep-ph/9906273].
#

import math
import sys

if (len(sys.argv[1:2]) == 1):
    m_t_arr=sys.argv[1:2]
else:
    m_t_arr=[ 140, 150, 160, 165, 167.5, 170, 170.5, 171, 171.5, 172, 172.5, 173, 173.5, 174, 174.5, 175, 177.5, 180, 190, 200, 210 ]


# ATLAS MC11 conventions == PDG2010 
# Vtb=0.999152 
# using Vtb=1.0 since part of the inputs was already produced using this approximation 
Vtb=1.0
M_W=80.399
# PDG2010 
G_F=1.16637*(math.pow(10,-5))
# MSbar alpha_s(mt)
alpha_s=0.108
# Born gamma coeff. 
C1=G_F/(8*math.pi*math.sqrt(2))

for m_t in m_t_arr:
    # Born approximation (taking intermediate W-boson to be on-shell) [1]
    Gamma_B=C1*math.pow(float(m_t),3)*math.pow(Vtb,2)*pow((1-math.pow((M_W/float(m_t)),2)),2)*(1+2*pow((M_W/float(m_t)),2))
    
    # EW and QCD corrections to Born: QCD dominates, EW can be neglected [1],[2],[3]
    Gamma=Gamma_B*(1-0.81*alpha_s-1.81*pow(alpha_s,2))
    
    print ("%.1f" % m_t), ("%.3f" % Gamma)

sys.exit(0)

## 140.0 0.598
## 150.0 0.790
## 160.0 1.008
## 165.0 1.127
## 167.5 1.189
## 170.0 1.254
## 172.5 1.320
## 175.0 1.388
## 177.5 1.458
## 180.0 1.529
## 190.0 1.836
## 200.0 2.177
## 210.0 2.551
