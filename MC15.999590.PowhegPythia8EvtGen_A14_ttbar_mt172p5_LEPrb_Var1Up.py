#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'POWHEG+Pythia8 ttbar production with Powheg hdamp equal 1.5*top mass, A14 tune, at least one lepton filter, ME NNPDF30 NLO, A14 NNPDF23 LO'
evgenConfig.keywords    = [ 'SM', 'top', 'ttbar', 'lepton']
evgenConfig.contact     = [ 'simone.amoroso@cern.ch','marco.vanadia@cern.ch']

include('PowhegControl/PowhegControl_tt_Common.py')
# Initial settings
#PowhegConfig.topdecaymode = 22222                                         # Inclusive
if hasattr(PowhegConfig, "topdecaymode"):
    # Use PowhegControl-00-02-XY (and earlier) syntax
    PowhegConfig.topdecaymode = 22222 # inclusive top decays
else:
    # Use PowhegControl-00-03-XY (and later) syntax
    PowhegConfig.decay_mode = "t t~ > all"

PowhegConfig.mass_t = 172.5
PowhegConfig.width_t = 1.320	#from the script get_t_width.py
PowhegConfig.hdamp        = 258.75                                        # 1.5 * mtop
PowhegConfig.mu_F         = [1.0]
PowhegConfig.mu_R         = [1.0]
PowhegConfig.PDF          = [260000]

PowhegConfig.nEvents     *= 15. # compensate filter efficiency
PowhegConfig.generate()

#--------------------------------------------------------------
# Pythia8 showering
#--------------------------------------------------------------

include('MC15JobOptions/Pythia8_A14_NNPDF23LO_Var1Up_EvtGen_Common.py')
include("MC15JobOptions/Pythia8_Powheg_Main31.py")
genSeq.Pythia8.UserModes += [ 'Main31:pTHard = 0' ]
genSeq.Pythia8.UserModes += [ 'Main31:NFinal = 2' ]
genSeq.Pythia8.UserModes += [ 'Main31:pTdef = 2' ]
genSeq.Pythia8.UserModes += [ 'Main31:veto = 1' ]
genSeq.Pythia8.UserModes += [ 'Main31:vetoCount = 3' ]
genSeq.Pythia8.UserModes += [ 'Main31:pTemt  = 0' ]
genSeq.Pythia8.UserModes += [ 'Main31:emitted = 0' ]
genSeq.Pythia8.UserModes += [ 'Main31:MPIveto = 0' ]
#rB value fitted at LEP=1.05
genSeq.Pythia8.Commands += [ 'StringZ:rFactB = 1.05' ]


#--------------------------------------------------------------
# Event filter
#--------------------------------------------------------------
include('Adhoclepfilt.py')

