#!/bin/bash

dsNumber=$1
dsName=$2
nJobs=$3
numEvents=$4
version=$5
script=${6:-generate_PP8.sh}

#./$script $dsNumber $dsName $numEvents 1234  $USER
#echo prun --exec "$script $dsNumber $jo $nJobs $numEvents %RNDM:1322347 $USER" --disableAutoRetry --nJobs $nJobs --outputs DAOD_TRUTH1.$dsName.root nJobs --outDS user.$USER.TQPMC.$dsName.$5 --excludedSite ANALY_LANCS_SL7 --extFile=.gz
prun --exec "$script $dsNumber $dsName $numEvents %RNDM:1322347 $USER" --disableAutoRetry --nJobs $nJobs --outputs DAOD_TRUTH1.file.root nJobs --outDS user.$USER.TQPMC.$dsName.$5 --excludedSite ANALY_LANCS_SL7 --extFile=.gz #--express


