# arXiv:1303.6254
#
# \sigma(m) = \sigma(m_ref) (m_ref / m)^4 \times (1 + a_1 ((m-m_ref)/m_ref) + a_2 ((m-m_ref)/m_ref)^2)
#
#
#


def xsect_vs_mt(  x,  par ):
    # par[0] = m_ref
    # par[1] = sigma(m_ref)
    # par[2] = a_1
    # par[3] = a_2
    sigma= (par[1])*((par[0]/x)**4)*(1+par[2]*(x-par[0])/(par[0]) + par[3]*(((x-par[0])/(par[0]))**2) )
    return sigma

param=[172.5,823.441,-0.708529,0.213963]


for mass in [165,170,170.5,171,171.5,172,172.5,173,173.5,174,174.5,175,180]:
	print mass,xsect_vs_mt(mass,param)*831.76/xsect_vs_mt(172.5,param)

