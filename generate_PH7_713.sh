#!/bin/bash                                                                                                                                                   
echo "Got $*"
ulimit -a

dsNumber=$1
dsName=$2
numEvents=$3
seed=$4
usname=$5

jo=MC15.$dsNumber.$dsName.py

export AtlasSetup=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/AtlasSetup/current/AtlasSetup
#source $AtlasSetup/scripts/asetup.sh  AtlasProduction,19.2.5.5,standalone #version used for official ttbar
#source $AtlasSetup/scripts/asetup.sh  AtlasProduction,19.2.5.32,standalone
#source $AtlasSetup/scripts/asetup.sh  MCProd,19.2.5.33.1,standalone Py8.212
#source $AtlasSetup/scripts/asetup.sh  MCProd,19.2.5.31.1,standalone #Py8.230 and weights
#source $AtlasSetup/scripts/asetup.sh  MCProd,19.2.5.12.2,standalone Sherpa
source $AtlasSetup/scripts/asetup.sh  MCProd,20.7.9.9.13,standalone #H7.1.3

export TMPDIR=$PWD
export TMP=$PWD
export GFORTRAN_TMPDIR=$PWD

echo "input parameters"
echo $dsNumber
echo $dsName
echo $numEvents
echo $seed
echo $usname
echo $jo

Generate_tf.py --ecmEnergy=13000. --runNumber=$dsNumber --firstEvent=1 --maxEvents=$numEvents --randomSeed=$seed --jobConfig=$jo --outputEVNTFile=user.$usname.pool.root  
source $AtlasSetup/scripts/asetup.sh --cmtconfig=x86_64-slc6-gcc49-opt AtlasDerivation,20.7.8.24,standalone #corresponds to p3317
Reco_tf.py --inputEVNTFile=user.$usname.pool.root  --outputDAODFile file.root --reductionConf TRUTH1 --preExec='rec.doApplyAODFix.set_Value_and_Lock(False)' --passThrough True
# --preExec "default:from BTagging.BTaggingFlags import BTaggingFlags;BTaggingFlags.CalibrationTag = \"BTagCalibRUN12-08-18\""
#source $AtlasSetup/scripts/asetup.sh AtlasDerivation,20.7.9.6,gcc49,here
#Reco_tf.py --inputEVNTFile=user.mvanadia.pool.root --outputDAODFile $outName.root --reductionConf TRUTH1
