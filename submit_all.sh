#./submit.sh DSNUMBER DSNAME NJOBS NEVENTS_PER_JOB SUFFIX SCRIPT
#SCRIPT is generate_PP8.sh for PP8, generate_PH7_713.sh for P+Herwig7.1.3
SUFFIX="v4"

#note: in v2 fixed JOs for RadHi and RadLow samples
#note: in v3 fixed JOs for mass variation samples

#nominal no rb fit
#./submit.sh 999500 PowhegPythia8EvtGen_A14_ttbar_mt172p5 4000 50000 $SUFFIX generate_PP8.sh #200 mln
#./submit.sh 999602 PowhegPythia8EvtGen_A14_ttbar_mt170p0 2000 25000 $SUFFIX generate_PP8.sh #50 mln
#./submit.sh 999612 PowhegPythia8EvtGen_A14_ttbar_mt175p0 2000 25000 $SUFFIX generate_PP8.sh #50 mln
##nominal rb fit
#./submit.sh 999507 PowhegPythia8EvtGen_A14_ttbar_mt172p5_LEPrb 4000 50000 $SUFFIX generate_PP8.sh #200 mln
##mass variations
#./submit.sh 999502 PowhegPythia8EvtGen_A14_ttbar_mt170p0_LEPrb 2000 25000 $SUFFIX generate_PP8.sh #50 mln
#./submit.sh 999503 PowhegPythia8EvtGen_A14_ttbar_mt170p5_LEPrb 2000 25000 $SUFFIX generate_PP8.sh #50 mln
#./submit.sh 999504 PowhegPythia8EvtGen_A14_ttbar_mt171p0_LEPrb 2000 25000 $SUFFIX generate_PP8.sh #50 mln
#./submit.sh 999505 PowhegPythia8EvtGen_A14_ttbar_mt171p5_LEPrb 2000 25000 $SUFFIX generate_PP8.sh #50 mln
#./submit.sh 999506 PowhegPythia8EvtGen_A14_ttbar_mt172p0_LEPrb 2000 25000 $SUFFIX generate_PP8.sh #50 mln
#./submit.sh 999508 PowhegPythia8EvtGen_A14_ttbar_mt173p0_LEPrb 2000 25000 $SUFFIX generate_PP8.sh #50 mln
#./submit.sh 999509 PowhegPythia8EvtGen_A14_ttbar_mt173p5_LEPrb 2000 25000 $SUFFIX generate_PP8.sh #50 mln
#./submit.sh 999510 PowhegPythia8EvtGen_A14_ttbar_mt174p0_LEPrb 2000 25000 $SUFFIX generate_PP8.sh #50 mln
#./submit.sh 999511 PowhegPythia8EvtGen_A14_ttbar_mt174p5_LEPrb 2000 25000 $SUFFIX generate_PP8.sh #50 mln
#./submit.sh 999512 PowhegPythia8EvtGen_A14_ttbar_mt175p0_LEPrb 2000 25000 $SUFFIX generate_PP8.sh #50 mln
#./submit.sh 999513 PowhegPythia8EvtGen_A14_ttbar_mt165p0_LEPrb 2000 25000 $SUFFIX generate_PP8.sh #50 mln
#./submit.sh 999514 PowhegPythia8EvtGen_A14_ttbar_mt180p0_LEPrb 2000 25000 $SUFFIX generate_PP8.sh #50 mln

#./submit.sh 999702 PowhegPythia8EvtGen_A14_ttbar_mt170p0 2000 25000 $SUFFIX generate_PP8.sh #50 mln
#./submit.sh 999703 PowhegPythia8EvtGen_A14_ttbar_mt170p5 2000 25000 $SUFFIX generate_PP8.sh #50 mln
#./submit.sh 999704 PowhegPythia8EvtGen_A14_ttbar_mt171p0 2000 25000 $SUFFIX generate_PP8.sh #50 mln
#./submit.sh 999705 PowhegPythia8EvtGen_A14_ttbar_mt171p5 2000 25000 $SUFFIX generate_PP8.sh #50 mln
#./submit.sh 999706 PowhegPythia8EvtGen_A14_ttbar_mt172p0 2000 25000 $SUFFIX generate_PP8.sh #50 mln
#./submit.sh 999708 PowhegPythia8EvtGen_A14_ttbar_mt173p0 2000 25000 $SUFFIX generate_PP8.sh #50 mln
#./submit.sh 999709 PowhegPythia8EvtGen_A14_ttbar_mt173p5 2000 25000 $SUFFIX generate_PP8.sh #50 mln
#./submit.sh 999710 PowhegPythia8EvtGen_A14_ttbar_mt174p0 2000 25000 $SUFFIX generate_PP8.sh #50 mln
#./submit.sh 999711 PowhegPythia8EvtGen_A14_ttbar_mt174p5 2000 25000 $SUFFIX generate_PP8.sh #50 mln
#./submit.sh 999712 PowhegPythia8EvtGen_A14_ttbar_mt175p0 2000 25000 $SUFFIX generate_PP8.sh #50 mln

##ISR
#./submit.sh 999520 PowhegPythia8EvtGen_A14_ttbar_mt172p5_LEPrb_RadHi 4000 50000 $SUFFIX generate_PP8.sh #200 mln
#./submit.sh 999521 PowhegPythia8EvtGen_A14_ttbar_mt172p5_LEPrb_RadLow 4000 50000 $SUFFIX generate_PP8.sh #200 mln
#./submit.sh 999720 PowhegPythia8EvtGen_A14_ttbar_mt172p5_RadHi 4000 50000 $SUFFIX generate_PP8.sh #200 mln
#./submit.sh 999721 PowhegPythia8EvtGen_A14_ttbar_mt172p5_RadLow 4000 50000 $SUFFIX generate_PP8.sh #200 mln
##PH7 7.0.4
#./submit.sh 999524 PowhegHerwig7EvtGen_H7UE_tt_hdamp258p75 4000 50000 $SUFFIX generate_PH7_704.sh #200 mln
##PH7 7.1.3
#./submit.sh 999525 PowhegHerwig7EvtGen_tt 4000 50000 $SUFFIX generate_PH7_713.sh #200 mln
##PP8 a là aMCatNLO
#./submit.sh 999550 PowhegPythia8EvtGen_A14_ttbar_mt172p5_LEPrb_MECoff_globrec 4000 50000 $SUFFIX generate_PP8.sh #200 mln
#./submit.sh 999750 PowhegPythia8EvtGen_A14_ttbar_mt172p5_MECoff_globrec 4000 50000 $SUFFIX generate_PP8.sh #200 mln
##rB variations
#./submit.sh 999560 PowhegPythia8EvtGen_A14_ttbar_mt172p5_LEPrb_rbUp 4000 50000 $SUFFIX generate_PP8.sh #200 mln
#./submit.sh 999561 PowhegPythia8EvtGen_A14_ttbar_mt172p5_LEPrb_rbDown 4000 50000 $SUFFIX generate_PP8.sh #200 mln
#./submit.sh 999562 PowhegPythia8EvtGen_A14_ttbar_mt172p5_LEPrb_rb1p075 4000 50000 $SUFFIX generate_PP8.sh #200 mln
#./submit.sh 999563 PowhegPythia8EvtGen_A14_ttbar_mt172p5_LEPrb_rb1p025 4000 50000 $SUFFIX generate_PP8.sh #200 mln
./submit.sh 999564 PowhegPythia8EvtGen_A14_ttbar_mt172p5_LEPrb_rb1p071 4000 50000 $SUFFIX generate_PP8.sh #200 mln
./submit.sh 999565 PowhegPythia8EvtGen_A14_ttbar_mt172p5_LEPrb_rb1p029 4000 50000 $SUFFIX generate_PP8.sh #200 mln
##alpha_s FSR variations + rb refit
#./submit.sh 999570 PowhegPythia8EvtGen_A14_ttbar_mt172p5_LEPrb_FSRupRbRefit 4000 50000 $SUFFIX generate_PP8.sh #200 mln
#./submit.sh 999571 PowhegPythia8EvtGen_A14_ttbar_mt172p5_LEPrb_FSRdownRbRefit 4000 50000 $SUFFIX generate_PP8.sh #200 mln
##Monash tune with rB refitted at LEP
#./submit.sh 999580 PowhegPythia8EvtGen_Monash_ttbar_mt172p5_LEPrb 4000 50000 $SUFFIX generate_PP8.sh #200 mln
##Underlying event
#./submit.sh 999590 PowhegPythia8EvtGen_A14_ttbar_mt172p5_LEPrb_Var1Up 4000 50000 $SUFFIX generate_PP8.sh #200 mln
#./submit.sh 999591 PowhegPythia8EvtGen_A14_ttbar_mt172p5_LEPrb_Var1Down 4000 50000 $SUFFIX generate_PP8.sh #200 mln
##Color reconnection
#./submit.sh 999600 PowhegPythia8EvtGen_A14_ttbar_mt172p5_LEPrb_CRmax 4000 50000 $SUFFIX generate_PP8.sh #200 mln
#./submit.sh 999601 PowhegPythia8EvtGen_A14_ttbar_mt172p5_LEPrb_CRoff 4000 50000 $SUFFIX generate_PP8.sh #200 mln
##aMCatNLO sample
#./submit_aMC.sh 999464 aMcAtNloPy8EvtGen_MEN30NLO_A14N23LO_ttbar_noShWe_LEPrb 4000 50000 $SUFFIX #200 mln
