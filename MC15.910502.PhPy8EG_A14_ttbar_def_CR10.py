#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'POWHEG+Pythia8 ttbar production with Powheg hdamp equal 1.5*top mass, A14 tune, at least one lepton filter, ME NNPDF30 NLO, A14 NNPDF23 LO from DSID 410450 LHE files with Shower Weights added '
evgenConfig.keywords    = [ 'SM', 'top', 'ttbar', 'lepton']
evgenConfig.contact     = [ 'amoroso@cern.ch']


include("PowhegControl/PowhegControl_tt_Common.py")
#from PowhegControl import ATLASCommonParameters
#ATLASCommonParameters.mass_t  = 172.5
#ATLASCommonParameters.width_t = 1.254

include('PowhegControl/PowhegControl_tt_Common.py')
if hasattr(PowhegConfig, "topdecaymode"):
    # Use PowhegControl-00-02-XY (and earlier) syntax                                                                                            
    PowhegConfig.topdecaymode = 22222 # inclusive top decays                                                                                                 else:
    # Use PowhegControl-00-03-XY (and later) syntax                                                                                            
    PowhegConfig.decay_mode_top = "t t~ > all"
PowhegConfig.mass_t = 172.5

PowhegConfig.width_t = 1.254
PowhegConfig.hdamp        = 258.75
PowhegConfig.PDF = 260000
PowhegConfig.nEvents     *= 3.
PowhegConfig.generate()



#--------------------------------------------------------------
# Pythia8 showering
#--------------------------------------------------------------

#include('MC15JobOptions/Pythia8_Monash_NNPDF23LO_EvtGen_Common.py')
include('MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')
include("MC15JobOptions/Pythia8_Powheg_Main31.py")

genSeq.Pythia8.Commands += [ 'Powheg:veto = 1' ]
genSeq.Pythia8.Commands += [ 'Powheg:pTdef = 2' ]
genSeq.Pythia8.Commands += [ 'Powheg:emitted = 0' ]
genSeq.Pythia8.Commands += [ 'Powheg:pTemt  = 0' ]
genSeq.Pythia8.Commands += [ 'Powheg:pTHard = 0' ]
genSeq.Pythia8.Commands += [ 'Powheg:vetoCount = 3' ]
genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 2' ]
genSeq.Pythia8.Commands += [ 'Powheg:MPIveto = 0' ]

genSeq.Pythia8.Commands += [ 'ColourReconnection:range = 10' ]



#--------------------------------------------------------------
# Event filter
#--------------------------------------------------------------
include('MC15JobOptions/TTbarWToLeptonFilter.py')
filtSeq.TTbarWToLeptonFilter.NumLeptons = -1
filtSeq.TTbarWToLeptonFilter.Ptcut = 0.

#include('MC15JobOptions/SoftLeptonInJetFilter.py')
#filtSeq.SoftLeptonInJetFilter.
#m_jet_cone=0.4
#m_part_ID=
#m_part_EtaRange=3
#m_part_Ptmin=3
