# based on the JobOptions MC15.429304
 
# Provide config information
evgenConfig.generators    += ["Powheg", "Herwig7", "EvtGen"]
evgenConfig.tune           = "H7-UE-MMHT"
evgenConfig.description    = "PowhegBox+Herwig7 7.1.3 ttbar production with Powheg hdamp equal top mass, H7-UE-MMHT tune, at least one lepton filter, with EvtGen, nonAllHadSMT"
evgenConfig.keywords       = ['SM', 'top', 'ttbar', 'lepton']
evgenConfig.contact        = ['simone.amoroso@cern.ch','marco.vanadia@cern.ch']
evgenConfig.minevents = 1000
 
include('PowhegControl/PowhegControl_tt_Common.py')
# PowhegConfig.topdecaymode = 22222
#PowhegConfig.decay_mode = "t t~ > all"
#PowhegConfig.hdamp        = 172.5
PowhegConfig.hdamp   = 258.75
PowhegConfig.PDF     = 260000
PowhegConfig.mu_F    = 1.0
PowhegConfig.mu_R    = 1.0
# compensate filter efficiency
PowhegConfig.nEvents     *= 15.
# PowhegConfig.generateRunCard()
# PowhegConfig.generateEvents()
PowhegConfig.generate()
 
# initialize Herwig7 generator configuration for showering of LHE files
include("MC15JobOptions/Herwig7_LHEF.py")
 
# configure Herwig7
Herwig7Config.me_pdf_commands(order="NLO", name="NNPDF30_nlo_as_0118")
Herwig7Config.tune_commands()
Herwig7Config.lhef_powhegbox_commands(lhe_filename=runArgs.inputGeneratorFile, me_pdf_order="NLO")
Herwig7Config.add_commands("""
read Matchbox/Powheg-DipoleShower.in

cd /Herwig/DipoleShower
set DipoleShowerHandler:PowhegDecayEmission No
do DipoleShowerHandler:AddVariation h_RU_FU 2 2 Hard
do DipoleShowerHandler:AddVariation h_RU_F0 2 1 Hard
do DipoleShowerHandler:AddVariation h_R0_FU 1 2 Hard
do DipoleShowerHandler:AddVariation h_R0_FL 1 0.5 Hard
do DipoleShowerHandler:AddVariation h_RL_F0 0.5 1 Hard
do DipoleShowerHandler:AddVariation h_RL_FL 0.5 0.5 Hard
do DipoleShowerHandler:AddVariation s_RU_FU 2 2 Secondary
do DipoleShowerHandler:AddVariation s_RU_F0 2 1 Secondary
do DipoleShowerHandler:AddVariation s_R0_FU 1 2 Secondary
do DipoleShowerHandler:AddVariation s_R0_FL 1 0.5 Secondary
do DipoleShowerHandler:AddVariation s_RL_F0 0.5 1 Secondary
do DipoleShowerHandler:AddVariation s_RL_FL 0.5 0.5 Secondary
set DipoleShowerHandler:Detuning 2.0
""")
#set SplittingGenerator:Detuning 2.0
# add EvtGen
include("MC15JobOptions/Herwig7_EvtGen.py")
 
# run Herwig7
Herwig7Config.run()
 
## NonAllHad filter
include('MC15JobOptions/TTbarWToLeptonFilter.py')
filtSeq.TTbarWToLeptonFilter.NumLeptons = -1
filtSeq.TTbarWToLeptonFilter.Ptcut = 20000.

from GeneratorFilters.GeneratorFiltersConf import TTbarWToLeptonFilter

filtSeq += TTbarWToLeptonFilter("onetWlep")
filtSeq.onetWlep.NumLeptons = 1
filtSeq.onetWlep.Ptcut = 0.

filtSeq += TTbarWToLeptonFilter("twotWlep")
filtSeq.twotWlep.NumLeptons = 2
filtSeq.twotWlep.Ptcut = 0.

from GeneratorFilters.GeneratorFiltersConf import ParentChildFilter
filtSeq += ParentChildFilter("Wmu")
filtSeq.Wmu.PDGParent  = [24]
filtSeq.Wmu.PDGChild = [13]

filtSeq += ParentChildFilter("Wetau")
filtSeq.Wetau.PDGParent  = [24]
filtSeq.Wetau.PDGChild = [11,15]

from GeneratorFilters.GeneratorFiltersConf import MultiMuonFilter
filtSeq += MultiMuonFilter("OneMuonFilter")
filtSeq.OneMuonFilter.Ptcut = 3250.
filtSeq.OneMuonFilter.Etacut = 2.8
filtSeq.OneMuonFilter.NMuons = 1

filtSeq += MultiMuonFilter("TwoMuonsFilter")
filtSeq.TwoMuonsFilter.Ptcut = 3250.
filtSeq.TwoMuonsFilter.Etacut = 2.8
filtSeq.TwoMuonsFilter.NMuons = 2

filtSeq += MultiMuonFilter("ThreeMuonsFilter")
filtSeq.ThreeMuonsFilter.Ptcut = 3250.
filtSeq.ThreeMuonsFilter.Etacut = 2.8
filtSeq.ThreeMuonsFilter.NMuons = 3

from GeneratorFilters.GeneratorFiltersConf import ElectronFilter
filtSeq += ElectronFilter("ElectronFilter")
filtSeq.ElectronFilter.Ptcut = 20000.
filtSeq.ElectronFilter.Etacut = 2.8

from GeneratorFilters.GeneratorFiltersConf import MuonFilter
filtSeq += MuonFilter("MuonFilter")
filtSeq.MuonFilter.Ptcut = 20000.
filtSeq.MuonFilter.Etacut = 2.8

filtSeq.Expression="TTbarWToLeptonFilter and (ElectronFilter or MuonFilter) and ((onetWlep and ((Wmu and TwoMuonsFilter) or (Wetau and OneMuonFilter))) or (twotWlep and ( ((not Wmu) and OneMuonFilter) or (Wmu and Wetau and TwoMuonsFilter) or (Wmu and (not Wetau) and ThreeMuonsFilter) )) )"

