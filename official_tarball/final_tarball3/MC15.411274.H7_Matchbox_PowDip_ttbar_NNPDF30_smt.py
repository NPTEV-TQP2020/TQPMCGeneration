from Herwig7_i.Herwig7_iConf import Herwig7
from Herwig7_i.Herwig7ConfigMatchbox import Hw7ConfigMatchbox

genSeq += Herwig7()

## Provide config information
evgenConfig.generators += ["Herwig7"]
evgenConfig.description = "Herwig7 ttbar sample with NNPDF30 ME PDF"
evgenConfig.keywords    = ["SM","ttbar"]
evgenConfig.contact     = ["Andrea Knue"]

## initialize generator configuration object
generator = Hw7ConfigMatchbox(genSeq, runArgs, run_name="HerwigMatchbox", beams="pp")

## configure generator
generator.me_pdf_commands(order="NLO", name="NNPDF30_nlo_as_0118")
generator.shower_pdf_commands(order="NLO", name="NNPDF30_nlo_as_0118")
#generator.tune_commands()
generator.add_commands("""
##################################################
## Process selection
##################################################

## Model assumptions
read Matchbox/StandardModelLike.in

## Set the hard process
set /Herwig/MatrixElements/Matchbox/Factory:OrderInAlphaS 2
set /Herwig/MatrixElements/Matchbox/Factory:OrderInAlphaEW 0
do /Herwig/MatrixElements/Matchbox/Factory:Process p p -> t tbar

read Matchbox/OnShellTopProduction.in

##################################################
## Matrix element library selection
##################################################
read Matchbox/MadGraph-OpenLoops.in

cd /Herwig/MatrixElements/Matchbox
insert Factory:DiagramGenerator:ExcludeInternal 0 /Herwig/Particles/e-
insert Factory:DiagramGenerator:ExcludeInternal 0 /Herwig/Particles/nu_ebar
insert Factory:DiagramGenerator:ExcludeInternal 0 /Herwig/Particles/mu+
insert Factory:DiagramGenerator:ExcludeInternal 0 /Herwig/Particles/nu_mu
insert Factory:DiagramGenerator:ExcludeInternal 0 /Herwig/Particles/h0

##################################################
## Cut selection
## See the documentation for more options
##################################################

##################################################
## Scale choice
## See the documentation for more options
##################################################

cd  /Herwig/MatrixElements/Matchbox/
set Factory:ScaleChoice Scales/TopPairMTScale

##################################################
## Matching and shower selection
## Please also see flavour scheme settings
## towards the end of the input file.
##################################################

#read Matchbox/MCatNLO-DefaultShower.in
# read Matchbox/MCatNLO-DipoleShower.in
#read Matchbox/Powheg-DefaultShower.in
read Matchbox/Powheg-DipoleShower.in

#switch off NLO decay of the top (only works for Dipole shower)
set DipoleShowerHandler:PowhegDecayEmission No                                                                                                               


##################################################
## Run with the original POWHEG formalism
##################################################

# cd /Herwig/MatrixElements/Matchbox
# set MEMatching:RestrictPhasespace No
# set MEMatching:HardScaleProfile NULL

##################################################
## Veto scale variations
##################################################

# read Matchbox/MuQUp.in
# read Matchbox/MuQDown.in

##################################################
## muR/muF scale variations
##################################################

# read Matchbox/MuUp.in
# read Matchbox/MuDown.in

##################################################
## PDF selection
##################################################

cd /Herwig/Partons
create ThePEG::LHAPDF myPDFset ThePEGLHAPDF.so
set myPDFset:RemnantHandler HadronRemnants
set myPDFset:PDFName NNPDF30_nlo_as_0118
cd /Herwig/Couplings
set NLOAlphaS:input_scale 91.199997*GeV
set NLOAlphaS:input_alpha_s 0.118
set NLOAlphaS:max_active_flavours 5

cd /Herwig/Partons
set /Herwig/Particles/p+:PDF myPDFset
set /Herwig/Particles/pbar-:PDF myPDFset

set /Herwig/Partons/PPExtractor:FirstPDF myPDFset
set /Herwig/Partons/PPExtractor:SecondPDF myPDFset

set /Herwig/Shower/LtoLGammaSudakov:pTmin 0.000001
#set /Herwig/Shower/AlphaQCD:AlphaMZ
#set /Herwig/Shower/GtoQQbarSplitFn:AngularOrdered Yes
#set /Herwig/Shower/Evolver:MECorrMode 1
#set /Herwig/Shower/PartnerFinder:PartnerMethod Random
#set /Herwig/Shower/PartnerFinder:ScaleChoice Partner
#set /Herwig/Shower/ShowerHandler:RestrictPhasespace Yes
#set /Herwig/Shower/ShowerHandler:MaxPtIsMuF Yes
#set /Herwig/Shower/GammatoQQbarSudakov:Alpha /Herwig/Shower/AlphaQED
set /Herwig/Shower/QtoGammaQSudakov:Alpha /Herwig/Shower/AlphaQED

cd /Herwig/Particles
set t:NominalMass 172.5*GeV
set t:HardProcessMass 172.5*GeV
set t:Width 1.3167*GeV

set W+:NominalMass 80.399*GeV
set W+:HardProcessMass 80.399*GeV
set W+:Width 2.09974*GeV

set Z0:NominalMass 91.1876*GeV
set Z0:HardProcessMass 91.1876*GeV
set Z0:Width 2.50966*GeV

#set /Herwig/Model:EW/Scheme GMuScheme
#set /Herwig/Model:EW/FermiConstant 1.16637e-05
#set /Herwig/Model:EW/RecalculateEW On
#set /Herwig/MatrixElements/Matchbox/Factory:FixedQEDCouplings Yes

cd /Herwig/Analysis
set Basics:CheckQuark No

##################################################
## PDF choice
##################################################

#read Matchbox/FiveFlavourScheme.in
## required for dipole shower and fixed order in five flavour scheme
read Matchbox/FiveFlavourNoBMassScheme.in
""")

#Replicate authors NLO sampler commands in Interface
generator.sampler_commands("MonacoSampler", 20000, 4, 50000, 1, 100)

include("MC15JobOptions/Herwig7_EvtGen.py")

## run generator
generator.run(cleanup_herwig_scratch=True)

## NonAllHad filter
include('MC15JobOptions/TTbarWToLeptonFilter.py')
filtSeq.TTbarWToLeptonFilter.NumLeptons = -1
filtSeq.TTbarWToLeptonFilter.Ptcut = 20000.

from GeneratorFilters.GeneratorFiltersConf import TTbarWToLeptonFilter

filtSeq += TTbarWToLeptonFilter("onetWlep")
filtSeq.onetWlep.NumLeptons = 1
filtSeq.onetWlep.Ptcut = 0.

filtSeq += TTbarWToLeptonFilter("twotWlep")
filtSeq.twotWlep.NumLeptons = 2
filtSeq.twotWlep.Ptcut = 0.

from GeneratorFilters.GeneratorFiltersConf import ParentChildFilter
filtSeq += ParentChildFilter("Wmu")
filtSeq.Wmu.PDGParent  = [24]
filtSeq.Wmu.PDGChild = [13]

filtSeq += ParentChildFilter("Wetau")
filtSeq.Wetau.PDGParent  = [24]
filtSeq.Wetau.PDGChild = [11,15]

from GeneratorFilters.GeneratorFiltersConf import MultiMuonFilter
filtSeq += MultiMuonFilter("OneMuonFilter")
filtSeq.OneMuonFilter.Ptcut = 3250.
filtSeq.OneMuonFilter.Etacut = 2.8
filtSeq.OneMuonFilter.NMuons = 1

filtSeq += MultiMuonFilter("TwoMuonsFilter")
filtSeq.TwoMuonsFilter.Ptcut = 3250.
filtSeq.TwoMuonsFilter.Etacut = 2.8
filtSeq.TwoMuonsFilter.NMuons = 2

filtSeq += MultiMuonFilter("ThreeMuonsFilter")
filtSeq.ThreeMuonsFilter.Ptcut = 3250.
filtSeq.ThreeMuonsFilter.Etacut = 2.8
filtSeq.ThreeMuonsFilter.NMuons = 3

from GeneratorFilters.GeneratorFiltersConf import ElectronFilter
filtSeq += ElectronFilter("ElectronFilter")
filtSeq.ElectronFilter.Ptcut = 20000.
filtSeq.ElectronFilter.Etacut = 2.8

from GeneratorFilters.GeneratorFiltersConf import MuonFilter
filtSeq += MuonFilter("MuonFilter")
filtSeq.MuonFilter.Ptcut = 20000.
filtSeq.MuonFilter.Etacut = 2.8

filtSeq.Expression="TTbarWToLeptonFilter and (ElectronFilter or MuonFilter) and ((onetWlep and ((Wmu and TwoMuonsFilter) or (Wetau and OneMuonFilter))) or (twotWlep and ( ((not Wmu) and OneMuonFilter) or (Wmu and Wetau and TwoMuonsFilter) or (Wmu and (not Wetau) and ThreeMuonsFilter) )) )"
